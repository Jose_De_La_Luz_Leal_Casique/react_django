from django.urls import path
from . import views


urlpatterns = [
    path('register/', views.RegisterView.as_view(), name='register_user'),
    path('email_verify/', views.VerifyEmail.as_view(), name='email_verify'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('load_user/', views.GetUser.as_view(), name='load_user'),
]
