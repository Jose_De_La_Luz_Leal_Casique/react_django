from rest_framework import serializers
from rest_framework.exceptions import AuthenticationFailed

from rest_framework_simplejwt.tokens import RefreshToken, TokenError

from django.contrib import auth

from .models import User
from .utils import Validators


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email']


class RegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        max_length=30, min_length=8, write_only=True, validators=(Validators.validation_password,))

    default_error_messages = {
        'username': 'El nombre de usuario solo debe ser alfanumérico'}

    class Meta:
        model = User
        fields = ['email', 'username', 'password']

    def validate(self, attrs):
        email = attrs.get('email', '')
        username = attrs.get('username', '')

        if not username.isalnum():
            raise serializers.ValidationError(self.default_error_messages)

        return attrs

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class EmailVerificationSerializer(serializers.ModelSerializer):
    token = serializers.CharField(max_length=555)

    class Meta:
        model = User
        fileds = ['token']


class LoginSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(max_length=100, read_only=True)
    password = serializers.CharField(max_length=30, min_length=8, write_only=True)
    username = serializers.CharField(max_length=60)
    tokens = serializers.SerializerMethodField()

    def get_tokens(self, obj):
        user = User.objects.get(username=obj['username'])
        return {
            'refresh': user.tokens()['refresh'],
            'access': user.tokens()['access']}

    class Meta:
        model = User
        fields = ['email', 'username', 'password', 'tokens']

    def validate(self, attrs):
        username = attrs.get('username', '')
        password = attrs.get('password', '')

        if not User.objects.filter(username=username).exists():
            raise AuthenticationFailed(detail='Cuenta inexistente')

        user = auth.authenticate(username=username, password=password)

        if not user:
            raise AuthenticationFailed('Credenciales Invalidas, intente otra vez')

        if not user.is_active:
            raise AuthenticationFailed('Cuenta Bloqueda, Contacte con adinistrador')

        if not user.is_verified:
            raise AuthenticationFailed('Verificación de email no realizada')

        return {
            'email': user.email,
            'username': user.username,
            'tokens': user.tokens}

        return super().validate(attrs)


class LogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField()

    default_error_messages = {
        'bad_token': ('El Token expiró o es Inválido ')
    }

    def validate(self, attrs):
        self.token = attrs['refresh']
        return attrs

    def save(self, **kwargs):
        try:
            RefreshToken(self.token).blacklist()

        except TokenError:
            self.fail('bad_token')