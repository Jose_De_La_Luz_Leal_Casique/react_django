from django.contrib.sites.shortcuts import get_current_site
from django.urls import reverse
from django.conf import settings
from django.views import generic

from rest_framework import generics, status, permissions
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from . import serializers
from .models import User
from .utils import Util

import jwt


class GetUser(generics.RetrieveAPIView):
    permission_classes = [ permissions.IsAuthenticated, ]
    serializer_class = serializers.UserSerializer

    def get_object(self):
        return self.request.user


class RegisterView(generics.GenericAPIView):
    serializer_class = serializers.RegisterSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            user = serializer.save()
            token = RefreshToken.for_user(user).access_token
            current_site = get_current_site(request).domain
            link = reverse('email_verify')
            url = 'http://'+current_site+link+'?token='+str(token)
            email_body = 'Bienvenido '+user.username + \
                '. Use el siguiente link para verificar el correo electrónico: \n' +url
            data = {
                'email_body': email_body, 'to_email': user.email, 'email_subject': 'Confime su email'}
            Util.send_email(data)

            return Response({
                'user': self.get_serializer(user, context=self.get_serializer_context()).data
            }, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class VerifyEmail(generic.TemplateView):
    # serializer_class = serializers.EmailVerificationSerializer
    template_name = 'verify_email.html'

    # token_param_config = openapi.Parameter(
    #    'token', in_=openapi.IN_QUERY, description='Description', type=openapi.TYPE_STRING)

    # @swagger_auto_schema(manual_parameters=token_param_config)
    # def get(self, request):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        token = self.request.GET.get('token')
        validated = {}

        try:
            payload = jwt.decode(token, settings.SECRET_KEY)
            user = User.objects.get(id=payload['user_id'])

            if not user.is_verified:
                user.is_verified = True
                user.save()
                validated = {'email': 'Activación Correcta', 'status': 202}
                # return Response({'email': 'Activación Correcta'}, status=status.HTTP_200_OK)

            else:
                validated = {'email': 'Este Email ya Está Activo', 'status': 200}

        except jwt.ExpiredSignatureError:
            validated = {'email': 'Intento de Activación Fuera de Tiempo', 'status': 406}
            # return Response({'error': 'Activación Caducada'}, status=status.HTTP_400_BAD_REQUEST)

        except jwt.exceptions.DecodeError:
            validated = {'email': 'Token Invalido', 'status': 403}
            # return Response({'error': 'Token Invalido'}, status=status.HTTP_400_BAD_REQUEST)

        context['verify_email'] = validated
        print(context)
        return context


class LoginView(generics.GenericAPIView):
    serializer_class = serializers.LoginSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)


class LogoutView(generics.GenericAPIView):
    serializer_class = serializers.LogoutSerializer
    permission_classes = [permissions.IsAuthenticated, ]

    def post(self, request):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_204_NO_CONTENT)

        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)
