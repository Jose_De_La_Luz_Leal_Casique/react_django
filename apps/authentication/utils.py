from django.core.mail import EmailMessage
from rest_framework import serializers
import threading
import re


class Validators:

    @staticmethod
    def validation_password(password):
        match = re.search('(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,30}$', password)
        if not match:
            raise serializers.ValidationError(
                'La contraseña debe tener al más de 8 caracteres,' \
                'al menos un dígito, al menos una minúscula y al menos una mayúscula.')
        return password


class EmailThread(threading.Thread):

    def __init__(self, email):
        self.email = email
        threading.Thread.__init__(self)

    def run(self):
        self.email.send()


class Util:

    @staticmethod
    def send_email(data):
        email = EmailMessage(
            subject=data['email_subject'], body=data['email_body'], to=[data['to_email']])
        EmailThread(email).start()

