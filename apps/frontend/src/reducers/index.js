import { combineReducers } from 'redux';
import Auth from './auth';
import Errors from './errors';
import Messages from './messages';

export default combineReducers({
    Auth,
    Errors,
    Messages,
});
