import {
    REGISTER_SUCCESS, REGISTER_FAIL, LOGIN_SUCCESS, LOGIN_FAIL,
    USER_LOAD, USER_LOADING, AUTH_ERROR, LOGOUT_SUCCESS
} from '../actions/types';

const initialState = {
    isAuthenticated: false,
    inProcess: false,
    user: null
};

export default function (state = initialState, action) {
    switch (action.type) {
        case USER_LOADING:
            return {
                ...state,
                inProcess: true
            };

        case USER_LOAD:
            return {
                ...state,
                isAuthenticated: true,
                inProcess: false,
                user: action.payload,
            };

        case REGISTER_SUCCESS:
            return {
                ...state,
                inProcess: true
            };

        case REGISTER_FAIL:
        case AUTH_ERROR:
        case LOGIN_FAIL:
            return {
                ...state,
                inProcess: false
            };

        case LOGIN_SUCCESS:
            localStorage.setItem('access', action.payload.tokens.access);
            localStorage.setItem('refresh', action.payload.tokens.refresh);
            return {
                ...state,
                user: action.payload,
                isAuthenticated: true
            };

        case LOGOUT_SUCCESS:
            localStorage.setItem('access', '')
            localStorage.setItem('refresh', '')
            return {
                ...state,
                user: null,
                isAuthenticated: false
            };

        default:
            return state
    }
}