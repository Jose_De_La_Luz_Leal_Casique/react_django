import React , { Component, Fragment } from 'react';
import { withAlert } from 'react-alert';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';


export class Alerts extends Component {
    static proptypes = {
        error: PropTypes.object.isRequired,
        message: PropTypes.object.isRequired
    };

    componentDidUpdate(prevProps) {
        const { error, alert, message } = this.props;

        if (error !== prevProps.error){
            if (error.msg.username) alert.error(`Usuario: ${error.msg.username.join()}`);
            if (error.msg.password) alert.error(`Contraseña: ${error.msg.password.join()}`);
            if (error.msg.email) alert.error(`Correo: ${error.msg.email.join()}`);

        }

        if (message !== prevProps.message) {
            if (message.msg.successRegister) alert.success(message.msg.successRegister);
            if (message.msg.passwordNotMatch) alert.error(message.msg.passwordNotMatch);
        }
    }

    render() {
        return <Fragment />;
    }
}

const mapStateToProps = (state) => ({
    error: state.Errors,
    message: state.Messages
});

export default connect(mapStateToProps)(withAlert()(Alerts));
