import React, { Component, Fragment } from 'react';
import { /*BrowserRouter*/ HashRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

import { Provider as AlertProvider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';

import { Provider } from 'react-redux';
import store from '../store';

import Register from './accounts/Register';
import Login from './accounts/Login';
import Home from './pages/Home';

import Header from './layout/Header';
import Alerts from "./layout/Alerts";

import PrivateRoute from "./common/PrivateRoute";

import { loadDataUser } from '../actions/auth';


const alertOptions = {
    offset: '20px',
    timeout: 5000,
    position: 'top center'
};


class App extends Component {
    componentDidMount() {
        store.dispatch(loadDataUser());
    };

    render() {
        return (
            <Provider store={store}>
                <AlertProvider template={AlertTemplate} {...alertOptions}>
                    <Router>
                        <Fragment>
                            <Header/>
                            <Alerts />
                            <div className="container">
                                <Switch>
                                    <PrivateRoute exact path="/" component={Home} />
                                    <Route exact path="/register" component={Register} />
                                    <Route exact path="/login" component={Login} />
                                </Switch>
                            </div>
                        </Fragment>
                    </Router>
                </AlertProvider>
            </Provider>
        )
    }
}

export default App;
