import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser } from '@fortawesome/free-solid-svg-icons';

import { register } from '../../actions/auth';
import { createMessage } from '../../actions/messages';

export class Register extends  Component {
    state = {
        username: '',
        password: '',
        password2: '',
        email: ''
    };

    static propTypes = {
        register: PropTypes.func.isRequired,
        message: PropTypes.object.isRequired,
        inProcess: PropTypes.bool,
        isAuthenticated: PropTypes.bool
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    onSubmit = (e) => {
        console.log('Submit -> Registrar Usuario');
        e.preventDefault();
        const { username, email, password, password2 } = this.state;

        if (password !== password2) {
            this.props.createMessage({ passwordNotMatch: 'Las Contraseña no Coinciden' });

        } else {
            const dataUser = { username, email, password };
            const result = this.props.register(dataUser);

        }
    };

    render() {
        const {username, password, password2, email } = this.state;

        if (this.props.inProcess) return <Redirect to="/login" />;
        if (this.props.isAuthenticated) return <Redirect to="/" />;

        return (
            <div className="row mt-5">
                <div className="col-md-10 m-auto">
                    <div className="row forms">
                        <div className="section-image card card-body col-5">
                            <div className="container-icon">
                                <FontAwesomeIcon icon={faUser} size="6x" color="#1E8ADA"/>
                            </div>
                            <p className="text-image">Registro de Usuarios</p>
                        </div>
                        <div className="card col-7">
                            <form onSubmit={this.onSubmit} className="row section-form">
                                <div className="form-group col-10 m-auto">
                                    <input type="text" className="form-control input-site" name="username"
                                           required={true} placeholder="Nombre de usuario"
                                           onChange={this.onChange} value={username} />
                                </div>

                                <div className="form-group col-10 m-auto">
                                    <input type="email" className="form-control input-site" name="email"
                                           required={true} placeholder="Correo Electronico"
                                           onChange={this.onChange} value={email} />
                                </div>

                                <div className="form-group col-10 m-auto">
                                    <input type="password" className="form-control input-site" name="password"
                                           required={true} placeholder="Contraseña"
                                           onChange={this.onChange} value={password} />
                                </div>

                                <div className="form-group col-10 m-auto">
                                    <input type="password" className="form-control input-site" name="password2"
                                           required={true} placeholder="Confirmar Contraseña"
                                           onChange={this.onChange} value={password2} />
                                </div>

                                <div className="col-5 m-auto text-center container-btn">
                                    <button type="submit" className="btn btn-site">
                                        Registrar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    message: state.Messages,
    inProcess: state.Auth.inProcess,
    isAuthenticated: state.Auth.isAuthenticated
});

export default connect(mapStateToProps, { register, createMessage })(Register);