import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { loadDataUser, login } from '../../actions/auth';


export class Login extends  Component {

    state = { username: '', password: ''};

    static propTypes = {
        loadDataUser: PropTypes.func.isRequired,
        login: PropTypes.func.isRequired,
        isAuthenticated: PropTypes.bool,
        inProcess: PropTypes.bool
    };

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) => {
        e.preventDefault();
        this.props.login(this.state.username, this.state.password);
    }

    render() {
        if (this.props.isAuthenticated){
            return <Redirect to="/" />;
        }
        const { username, password } = this.state;

        return (
            <div className="col-md-6 m-auto">
                <div className="card card-body mt-5">
                    <h2 className="text-center">Login</h2>
                    <form onSubmit={this.onSubmit}>
                        <div className="form-group">
                            <label>Username</label>
                            <input type="text" className="form-control input-site" name="username"
                                   onChange={this.onChange} value={username} />
                        </div>

                        <div className="form-group">
                            <label>Password</label>
                            <input type="password" className="form-control input-site" name="password"
                            onChange={this.onChange} value={password} />
                        </div>

                        <div className="form-group">
                            <button type="submit" className="btn btn-primary">
                                Login
                            </button>
                        </div>
                        <p>
                            ¿No tienes cuenta? <Link to="/register">Registrate Aquí</Link>
                        </p>
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    inProcess: state.Auth.inProcess,
    isAuthenticated: state.Auth.isAuthenticated
});


export default connect(mapStateToProps, { loadDataUser, login })(Login);