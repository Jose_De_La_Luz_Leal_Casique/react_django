import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { register } from '../../actions/auth';

export class Register extends  Component {
    state = {
        imageProfile: '../../../static/frontend/images/images.png',
        image: null,
        key: '',
        name: '',
        paternal_surname: '',
        maternal_surname: '',
        username: '',
        password: '',
        password2: '',
        is_admin: 0
    };

    static propTypes = {
        register: PropTypes.func.isRequired,
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});
    checkedIsAdmin = (e) => this.setState({is_admin: e.target.checked === false ? 0: 1});

    fileSelected = (e) => {
        this.setState({image: e.target.files[0]});

        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState === 2) this.setState({imageProfile: reader.result});
        }
        reader.readAsDataURL(e.target.files[0])
    }

    onSubmit = (e) => {
        console.log('Submit -> Registrar Usuario');
        e.preventDefault();
        if (this.state.password !== this.state.password2 ) {
            alert('No coinciden las contraseñas');
            return false;
        }
        const data = new FormData();
        data.append('image', this.state.image, this.state.image.name);
        data.append('key', this.state.key);
        data.append('name', this.state.name);
        data.append('paternal_surname', this.state.paternal_surname);
        data.append('maternal_surname', this.state.maternal_surname);
        data.append('username', this.state.username);
        data.append('password', this.state.password);
        data.append('is_admin', this.state.is_admin);

        this.props.register(data);

    };

    render() {
        const {
            image, key, name, paternal_surname, maternal_surname, username, password, password2, is_admin } = this.state;

        return (
            <div className="row">
                <div className="col-md-10 col-lg-8 m-auto">
                    <div className="card card-head mt-4 alert-dark">
                        <h2 className="text-center">Registro de Usuario</h2>
                    </div>
                    <div className="card card-body mt-1 alert-primary">
                        <form onSubmit={this.onSubmit} className="row">
                            <div className="col-6">
                                <div className="form-group">
                                    <label>Nombre</label>
                                    <input type="text" className="form-control" name="name" required={true}
                                           onChange={this.onChange} value={name} />
                                </div>

                                <div className="form-group">
                                    <label>Apellido Paterno</label>
                                    <input type="text" className="form-control" name="paternal_surname" required={true}
                                           onChange={this.onChange} value={paternal_surname} />
                                </div>

                                <div className="form-group">
                                    <label>Apellido Materno</label>
                                    <input type="text" className="form-control" name="maternal_surname" required={true}
                                           onChange={this.onChange} value={maternal_surname} />
                                </div>

                                <div className="form-group">
                                    <label>Matricula o Número de Trabajador</label>
                                    <input type="text" className="form-control" name="key" required={true}
                                           onChange={this.onChange} value={key} />
                                </div>

                                <div className="form-group">
                                    <label>Es Administrador</label>
                                    <input type="checkbox" className="form-control"
                                           onChange={this.checkedIsAdmin} value={this.state.is_admin} />
                                </div>
                            </div>

                            <div className="col-6">
                                <div>
                                    <img src={this.state.imageProfile} className="img-profile"/>
                                </div>
                                <div className="form-group div-file">
                                    <label className="text-file">Agregar Imagen</label>
                                    <input type="file" className="form-control input-file" required={true}
                                           onChange={this.fileSelected} value={this.image} />

                                </div>

                                <div className="form-group">
                                    <label>Usuario</label>
                                    <input type="text" className="form-control" name="username" required={true}
                                           onChange={this.onChange} value={username} />
                                </div>

                                <div className="form-group">
                                    <label>Contraseña</label>
                                    <input type="password" className="form-control" name="password" required={true}
                                           onChange={this.onChange} value={password} />
                                </div>

                                <div className="form-group">
                                    <label>Confirmar Contraseña</label>
                                    <input type="password" className="form-control" name="password2" required={true}
                                           onChange={this.onChange} value={password2} />
                                </div>

                            </div>
                            <div className="col-5 m-auto">
                                <button type="submit" className="btn btn-primary">
                                    Registrar Usuario
                                </button>
                            </div>
                        </form>
                   </div>
                </div>
            </div>
        )
    }
}

export default connect(null, { register })(Register);