import axios from 'axios';
import {
    REGISTER_SUCCESS, REGISTER_FAIL, LOGIN_FAIL, LOGIN_SUCCESS, USER_LOAD,
    USER_LOADING, AUTH_ERROR, LOGOUT_SUCCESS
} from './types';
import { returnErrors, createMessage } from './messages';


export const loadDataUser = () => (dispatch, getState) => {
    dispatch({ type: USER_LOADING });

    axios.get('/authentication/load_user/', tokenAccess(getState))
        .then((res) => {
            dispatch({
               type:USER_LOAD,
               payload: res.data
            });
        })
        .catch((err) => {
            dispatch(returnErrors(err.response.data, err.response.status));
            dispatch({ type: AUTH_ERROR });
        });
};

/*
 * Módulo authentication
 * Register User
 * Sin configuración en peticiones application/json
 */
export const register = ({ username, email, password }) => (dispatch) => {

    const config = {
        'headers': {'Content-Type': 'application/json'}};

    const data = JSON.stringify({username, email, password});

    axios.post('/authentication/register/', data, config)
        .then((res) => {
            if (res.status === 400){
                dispatch( returnErrors(res.data));
                dispatch({ type: REGISTER_FAIL, payload: res.data });

            } else if (res.status === 200){
                dispatch(returnErrors(res.data));
                dispatch({ type: REGISTER_FAIL, payload: res.data });

            }else if (res.status === 201){
                dispatch(createMessage({ successRegister: 'Registro Correcto' }));
                dispatch({ type: REGISTER_SUCCESS, payload: res.data });
            }
        })
        .catch((err) => {
            dispatch(returnErrors(err.response.data, err.response.status));
            dispatch({ type:REGISTER_FAIL });
        });
};

/*
* Módulo authentication
* Login usuario
* application/json
*/

export const login = (username, password) => (dispatch) => {
    const config = {'headers': {'Content-Type': 'application/json'}}
    const data = JSON.stringify({username, password});

    axios.post('/authentication/login/', data, config)
        .then((res)=>{
            dispatch(createMessage({success_login: 'Sesión Iniciada'}));
            dispatch({
               type: LOGIN_SUCCESS,
               payload: res.data
            });
        })
        .catch((err) => {
            dispatch(returnErrors(err.response.data, err.response.status));
            dispatch({type: LOGIN_FAIL});
        });
};


export const logout = () => (dispatch) => {
    const refresh = localStorage.getItem('refresh');
    const data = JSON.stringify({refresh });

    axios.post('/authentication/logout/', data, tokenAccess())
        .then((res) => {
            dispatch({type: LOGOUT_SUCCESS});
        })
        .catch((err) =>{
            dispatch(returnErrors(err.response.data, err.response.status));
        });
};



export const tokenAccess = () => {
    const token = localStorage.getItem('access');
    const config = { 'headers': {'Content-Type': 'application/json'}};

    if (token)  config.headers['Authorization'] = `Bearer ${token}`;
    return config
};