from django.views import generic


class AppView(generic.TemplateView):
    template_name = 'frontend/index.html'


